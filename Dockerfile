# This file is a template, and might need editing before it works on your project.
FROM registry.gitlab.com/rockbox-player/base-build-container:master

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        libsdl1.2-dev \
    && rm -rf /var/lib/apt/lists/*
